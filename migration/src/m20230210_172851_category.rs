use sea_orm_migration::prelude::*;

use crate::m20220101_000001_create_table::Todo;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[derive(Iden)]
enum Category {
    Table,
    Id,
    Title,
}

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Replace the sample below with your own migration scripts

        manager
            .create_table(
                Table::create()
                    .table(Category::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Category::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Category::Title).string().not_null())
                    .to_owned(),
            )
            .await?;

        manager
            .alter_table(
                Table::alter()
                    .table(Todo::Table)
                    .add_column(ColumnDef::new(Todo::Category).integer())
                    .add_foreign_key(
                        TableForeignKey::new()
                            .name("FK_Todo_Category")
                            .from_tbl(Todo::Table)
                            .from_col(Todo::Category)
                            .to_tbl(Category::Table)
                            .to_col(Category::Id),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(Category::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(Category::Table).to_owned())
            .await
    }
}
