mod entity;
mod handler;

use std::net::SocketAddr;

use axum::{
    async_trait,
    routing::{delete, get, post}, Router,
};
use dotenv::dotenv;
use handler::*;
use sea_orm::{DatabaseConnection, Database};

#[tokio::main]
async fn main() {
    dotenv().ok();
    let db_url = dotenv::var("DATABASE_URL").unwrap();
    let db: DatabaseConnection = Database::connect(db_url).await.unwrap();

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    axum::Server::bind(&addr)
        .serve(router(db).into_make_service())
        .await
        .unwrap();
}

fn router(db: DatabaseConnection) -> Router {
    Router::new()
        .route("/todo/:id", get(handler::todo::select_one_todo))
        .route("/todo/", get(handler::todo::select_all_Todo))
        .route("/todo/delete/:id", delete(handler::todo::delete_todo))
        .route("/todo/new", post(handler::todo::new_todo))
        .route("/todo/update/:id", post(handler::todo::update_todo))
        .route("/category/", get(handler::category::select_all_Categorie))
        .route("/category/new", post(handler::category::new_category))
        .with_state(db)
}
