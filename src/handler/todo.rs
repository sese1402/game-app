
use std::net::SocketAddr;

use axum::{
    async_trait,
    extract::{Path, State},
    http::StatusCode,
    routing::{delete, get, post},
    Json, Router,
};
use dotenv::dotenv;
use crate::entity::prelude::*;
use sea_orm::{
    prelude::*, ActiveValue::NotSet, Database, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, QueryOrder, Set,
};

pub async fn select_one_todo(db: State<DatabaseConnection>, Path(id): Path<i32>) -> (StatusCode, String) {
    let element = Todo::find_by_id(id).all(&db.0).await.unwrap();

    let data = serde_json::to_string(&element).unwrap();
    (StatusCode::OK, data)
}

pub async fn select_all_Todo(db: State<DatabaseConnection>) -> (StatusCode, String) {
    let element = Todo::find().all(&db.0).await.unwrap();
    let data = serde_json::to_string(&element).unwrap();
   (StatusCode::OK, data)
}
pub async fn all_Todos_category(
    db: State<DatabaseConnection>,
    Path(categoryid): Path<i32>,
) -> StatusCode {
    Todo::find()
        .filter(todo::Column::Category.eq(categoryid))
        .all(&db.0)
        .await
        .unwrap();
    StatusCode::OK
}

pub async fn delete_todo(db: State<DatabaseConnection>, Path(id): Path<i32>) -> StatusCode {
    let res: DeleteResult = Todo::delete_by_id(id).exec(&db.0).await.unwrap();
    StatusCode::OK
}

pub async fn new_todo(db: State<DatabaseConnection>, Json(input): Json<TodoModel>) -> StatusCode {
    let active_model: TodoAm = TodoAm {
        id: NotSet,
        title: Set(input.title),
        text: Set(input.text),
        category: Set(input.category),
    };
    active_model.insert(&db.0).await.unwrap();
    StatusCode::OK
}

pub async fn update_todo(
    db: State<DatabaseConnection>,
    Path(id): Path<i32>,
    Json(input): Json<TodoModel>,
) -> StatusCode {
    let active_model: TodoAm = TodoAm {
        id: Set(id),
        title: Set(input.title),
        text: Set(input.text),
        category: Set(input.category),
    };
    match active_model.update(&db.0).await {
        Ok(_) => StatusCode::OK,
        Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
    }
}
