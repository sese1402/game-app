
use std::net::SocketAddr;

use axum::{
    async_trait,
    extract::{Path, State},
    http::StatusCode,
    routing::{delete, get, post},
    Json, Router,
};
use dotenv::dotenv;
use crate::entity::prelude::*;
use sea_orm::{
    prelude::*, ActiveValue::NotSet, Database, DatabaseConnection, DeleteResult, EntityTrait,
    QueryFilter, QueryOrder, Set,
};

pub async fn select_all_Categorie(db: State<DatabaseConnection>) -> (StatusCode, String) {
    let element = Category::find().all(&db.0).await.unwrap();
    let data = serde_json::to_string(&element).unwrap();
    (StatusCode::OK, data)
}

pub async fn new_category(db: State<DatabaseConnection>, Json(input): Json<CategoryModel>) -> StatusCode {
    let active_model: CategoryAm = CategoryAm {
        id: NotSet,
        title: Set(input.title),
    };
    active_model.insert(&db.0).await.unwrap();
    StatusCode::OK
}


